import { Component, Input, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'modular_calc';

  resultadoPadre: number;

  procesarResultado(resultado: number): void {
    this.resultadoPadre = resultado;
  }
}
