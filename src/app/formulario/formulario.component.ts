import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrl: './formulario.component.css'
})
export class FormularioComponent {
  num1: number;
  num2: number;
  @Output() resultadoSuma = new EventEmitter<number>();

  sumar(): void {
    let res = this.num1 + this.num2;
    this.resultadoSuma.emit(res);
  }
}
